<?php

namespace Serenata\Refactoring;

use RuntimeException;

/**
 * Indicates something went wrong during use statement insertion creation.
 *
 * @final
 */
class UseStatementInsertionCreationException extends RuntimeException
{
}
